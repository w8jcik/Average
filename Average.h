#ifndef __LIB_AVERAGE_H
#define __LIB_AVERAGE_H

// TODO: should be configurable at runtime

#define AVERAGE_ITEM_NO 5

class Average {
  
  bool initialized;
  int values[AVERAGE_ITEM_NO];
  int pointer;
  
  public:
    Average(void);
    void write(int value);
    int read(void);
};

#endif  /* __LIB_AVERAGE_H */

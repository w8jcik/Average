#include "Average.h"

void fill_n(int arr[], int n, int val) {
  int i;
  
  for (i = 0; i < n; i += 1) {
    arr[i] = val;
  }
}

// TODO: write tests

Average::Average() {
  initialized = false;
  pointer = 0;
}
    
void Average::write(int value) {
  if (initialized) {
    values[pointer] = value;
  }
  else {
    fill_n(values, AVERAGE_ITEM_NO, value);
    initialized = true;
  }
  
  if (pointer == AVERAGE_ITEM_NO-1) {
    pointer = 0;
  }
  else {
    pointer += 1;
  }
}

int Average::read() {
  int i, acc;
  
  acc = values[0];
  
  for (i = 1; i < AVERAGE_ITEM_NO; i++) {
    acc += values[i];
  }
  
  return (acc / AVERAGE_ITEM_NO);
}
